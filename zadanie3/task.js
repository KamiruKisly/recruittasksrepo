const fnR = (n, result = "*", i = 0) => {

   if (n === 1) {
      console.log(' '.repeat(n + 1) + result.repeat(2 * i + 1))

      return;
   }

   console.log(' '.repeat(n + 1) + result.repeat(2 * i + 1));
   i++;

   return fnR(n - 1, "*", i);
}

fnR(5);