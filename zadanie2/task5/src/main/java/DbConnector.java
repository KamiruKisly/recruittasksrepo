import Model.Temp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public final class DbConnector {
    private static final String DB_CONNECTION = "jdbc:mysql://localhost/klienci?serverTimezone=CET";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "root";

    private static Connection connection;

    public static void connect() {
        try {
            connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static List<Temp> executeQuery(String q) throws SQLException {
        Statement stm = connection.createStatement();
        var result = stm.executeQuery(q);

        List<Temp> list = new ArrayList<>();

        while (result.next()){
            list.add(new Temp(result.getInt("id"), result.getTimestamp("time").toLocalDateTime(), result.getDouble("measurement")));
        }

        return list;
    }


}
