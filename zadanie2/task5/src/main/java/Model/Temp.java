package Model;

import java.time.LocalDateTime;

public class Temp {

    private int id;
    private LocalDateTime time;
    private Double measurement = null;

    public Temp(int id, LocalDateTime time, Double measurement) {
        this.id = id;
        this.time = time;

        if (measurement != 0.0) {
            this.measurement = measurement;
        }
    }

    public int getId() {
        return id;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public Double getMeasurement() {
        return measurement;
    }

}
