import Model.Temp;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws SQLException {

        DbConnector.connect();
        List<Temp> tempList = DbConnector.executeQuery("Select * from klienci.temp");

        getSunsetSunrise(tempList);
    }

    private static void getSunsetSunrise(List<Temp> list) {

        var collect = list.stream()
                .filter(temp -> temp.getMeasurement() != null)
                .collect(Collectors.groupingBy(temp -> temp.getTime().getDayOfMonth()));


        Stream<LocalDateTime> sunset = collect.values().stream()
                .map(temps -> temps.stream().map(Temp::getTime).max(LocalDateTime::compareTo).get());

        Stream<LocalDateTime> sunrise = collect.values().stream()
                .map(temps -> temps.stream().map(Temp::getTime).min(LocalDateTime::compareTo).get());

        System.out.println("Sunset:");
        sunset.forEach(localDateTime -> System.out.println(localDateTime.getHour() + ":" + localDateTime.getMinute()));

        System.out.println("sunrise:");
        sunrise.forEach(localDateTime -> System.out.println(localDateTime.getHour() + ":" + localDateTime.getMinute()));
    }
}
