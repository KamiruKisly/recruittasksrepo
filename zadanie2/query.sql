
1)
CREATE TABLE `temp` (
  `id` int(11)  NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `measurement` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

2)
-- Wschody i zachody założyłem zgodnie z pogodą, natomiast temperature ptraktowałem jako losowy double

DROP PROCEDURE InsertLoop;

DELIMITER $$
CREATE PROCEDURE InsertLoop()
BEGIN
	DECLARE x  INT;
    DECLARE temp DOUBLE;
	DECLARE stamp timestamp;
    DECLARE sunrise time;
    DECLARE sunset time;
        
	SET x = 0;
    SET temp = round(rand() * -4.00 + 4.55, 2);
    SET sunrise = '06:48:00';
    SET sunset = '17:11:00';
	SET stamp = "2021-02-19 00:00:00";
    
        
	loop_label:  LOOP
		IF  x =  4320 THEN 
			LEAVE  loop_label;
		END  IF;
        
		IF  TIME(stamp) < sunrise OR TIME(stamp) > sunset THEN 
			SET temp = null;
		END  IF;
        
 		IF   x =  1440 THEN 
             SET sunrise = '06:47:00';
 			SET sunset = '17:13:00';
 		END  IF;
         
       IF x =  2880 THEN 
			SET sunrise = '06:45:00';
 			SET sunset = '17:14:00';
 		END  IF;
        
 	INSERT INTO `klienci`.`temp`(`time`,`measurement`)
	VALUES(stamp ,temp);
    SET temp = round(rand() * -4.00 + 4.55, 2);
    SET stamp =ADDTIME(stamp, "0:1:00");
	
    set x = x+1;


	END LOOP;
END$$

DELIMITER ;

CALL InsertLoop();


3) SELECT MIN(TIME(temp.time)) as sunrise, MAX(TIME(temp.time)) as sunset FROM klienci.temp 
 WHERE temp.measurement IS NOT NULL
 GROUP BY DATE(temp.time)



4) Zakładając, że baza nie będzie się rozrastać na nowe tabele ale będzie przybywać danych z każdym dniem.  W takim przypadku dla duzej ilosćiu danych trzymanych w jednej tabeli/dokumencie/obiekcie nie
 majacych relacji z innymi danymi lepej zastosować bazy NoSQL jak np. MonogoDB gdyż bazy NoSQL są wydajniesze do przechowywania prostych struktrur w duzej ilośći
    

5) Kod znajduje się w folderze Task5