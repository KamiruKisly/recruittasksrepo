-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2021 at 08:30 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `klienci`
--

-- --------------------------------------------------------

--
-- Table structure for table `notatki`
--

CREATE TABLE `notatki` (
  `id` int(11) NOT NULL,
  `tytul` varchar(45) NOT NULL,
  `tresc` varchar(45) NOT NULL,
  `dataDodania` date NOT NULL,
  `kontoID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notatki`
--

INSERT INTO `notatki` (`id`, `tytul`, `tresc`, `dataDodania`, `kontoID`) VALUES
(1, 'testTytul', 'przyklad', '2021-02-26', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `notatki`
--
ALTER TABLE `notatki`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kontoID` (`kontoID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `notatki`
--
ALTER TABLE `notatki`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `notatki`
--
ALTER TABLE `notatki`
  ADD CONSTRAINT `notatki_ibfk_1` FOREIGN KEY (`kontoID`) REFERENCES `konto` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
